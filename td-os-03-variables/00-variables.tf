# ===
# Variable définitions
# ===

variable "external_network" {
  type    = string
  default = "external"
}

variable "external_router" {
  type    = string
  default = "ext"
}

# No default to force user to set the value
variable "dns_ip" {
  type = list(string)
  #default = ["208.67.222.222", "208.67.220.220"]
}

variable "user_ssh_pub_key" {
  type    = string
  default = ""
}

variable "subnet_ip_range" {
  type    = string
  default = "172.17.29.0/24"
}

variable "ingress_allow_sec_port" {
  type = list(object({
    port = string
    protocol = string }
    )
  )
  default = [
    #{ 
    #  port = "port_min:port_max"
    #  protocol = "tcp/udp"
    #}
  ]
}

variable "instance_name" {
  type    = string
  default = "instance"
}

variable "image_name" {
  type    = string
  default = "imta-ubuntu-basic"
}

variable "flavor_name" {
  type    = string
  default = "m1.small"
}

variable "key_pair_name" {
  type    = string
  default = "id_rsa"
}

variable "key_pair_pubkey" {
  type    = string
  default = ""
}
