# ===
# Security Groups
# ===

# allow ingress ssh and http tcp protocol 
resource "openstack_networking_secgroup_v2" "secgroup_http_ssh" {
  name = "http_ssh"
}
resource "openstack_networking_secgroup_rule_v2" "sg_rules_ssh" {
  # port => protocol because we need uniq key whereas protocol may be considered as duplicate
  for_each           = { for rule in var.ingress_allow_sec_port: rule.port => rule.protocol }
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = each.value
  port_range_min    = tonumber(split(":", each.key)[0])
  port_range_max    = tonumber(split(":", each.key)[0])
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_http_ssh.id}"
}


#resource "openstack_compute_secgroup_v2" "ingress_allow" {
#  name        = "ingress_allow"
#  description = "Open input ports"
#
#  dynamic "rule" {
#    for_each = { for rule in var.ingress_allow_sec_port : rule.port => rule.protocol }
#    iterator = rule
#    content {
#      from_port   = rule.key
#      to_port     = rule.key
#      ip_protocol = rule.value
#      cidr        = "0.0.0.0/0"
#    }
#  }
#}