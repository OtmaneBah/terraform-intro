output "ext_IP" {
  description = "IP address of floating IP"
  value       = openstack_networking_floatingip_v2.fip_http.address
}