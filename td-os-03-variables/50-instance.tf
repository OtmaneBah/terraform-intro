data "template_file" "user_data" {
  template = file("./scripts/add-ssh.yaml")
  vars = {
    user_ssh_pub_key = var.key_pair_pubkey
  }
}

resource "openstack_compute_instance_v2" "std_srv" {
  name            = var.instance_name
  image_name      = var.image_name
  flavor_name     = var.flavor_name
  key_pair        = openstack_compute_keypair_v2.user_key.name
  security_groups = ["http_ssh", "default"]
  user_data       = data.template_file.user_data.rendered
  network {
    name = openstack_networking_network_v2.internal_net.name
  }
}

# Attach floating ip to instance
resource "openstack_compute_floatingip_associate_v2" "http" {
  floating_ip = openstack_networking_floatingip_v2.fip_http.address
  instance_id = openstack_compute_instance_v2.std_srv.id
}
