
# ====
# variables
# ====
variable "nb_instance" {
  type    = string
  default = 2
}
variable "user_ssh_pub_key" {
  type    = string
  default = ""
}
variable "dns_ip" {
  type    = list(string)
  default = ["192.44.75.10", "192.108.115.2", ]
}

# ====
# Call module
# ====
# create os instance
module "deploy-os-instance" {
  nb_node       = var.nb_instance
  source        = "./os-pub-instance/"
  instance_name = "srv"
  #enable_ext_net   = true
  #enable_fip       = true
  external_network = "external"
  external_router  = "ms-icd-intro_03_router"
  subnet_ip_range  = "172.17.29.0/24"
  ingress_allow_sec_port = [
    {
      port     = "80:80"
      protocol = "tcp"
    },
    {
      port     = "22:22"
      protocol = "tcp"
    },
  ]
  image_name      = "imta-ubuntu"
  flavor_name     = "m1.small"
  dns_ip          = var.dns_ip
  key_pair_name   = "ced_ed25519"
  key_pair_pubkey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII9qbdHYYjYtqIatbemXRs7kaCR6HSqS7oVsSRIlFwYY user@SYS-Ubuntu20"
  #ansible_user    = var.username
  #ansible_ssh_key = var.user_ssh_pub_key
}


output "ext_IP" {
  value = module.deploy-os-instance.ext_IP
}
