data "template_file" "user_data" {
  template = file("${path.module}/scripts/add-ssh.yaml")
  vars = {
    user_ssh_pub_key = var.key_pair_pubkey
  }
}

resource "openstack_compute_instance_v2" "std_srv" {
  count           = var.nb_node
  name            = "${format("%s-%02d", var.instance_name, count.index + 1)}"
  image_name      = var.image_name
  flavor_name     = var.flavor_name
  key_pair        = openstack_compute_keypair_v2.user_key.name
  security_groups = ["ingress_allow", "default"]
  user_data       = data.template_file.user_data.rendered
  network {
    name = openstack_networking_network_v2.internal_net.name
  }
}

# Attach floating ip to instance
resource "openstack_compute_floatingip_associate_v2" "http" {
  count       = var.nb_node
  floating_ip = element(openstack_networking_floatingip_v2.fip_http.*.address, count.index)
  instance_id = element(openstack_compute_instance_v2.std_srv.*.id, count.index)
}
