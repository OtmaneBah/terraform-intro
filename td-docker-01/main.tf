terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_image" "goif" {
  name = "flower/goif"
}
resource "docker_image" "caddy" {
  name = "caddy"
}


resource "docker_container" "web" {
  name  = "web01"
  image = docker_image.caddy.latest

  ports {
    internal = "8080"
    external = "8096"
  }
}
