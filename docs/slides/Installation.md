---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->
![bg left:40% 100%](img/terraform.png)



## Terraform - Installation et premiers usages


-cedric.roger@seanops.com-

---

# Licence informations


Auteur : cedric.roger@seanops.com

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)


---

## Référence

[Documentation Terraform HashiCorp](https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/aws-get-started)
[](https://acloudguru.com/blog/engineering/the-ultimate-terraform-cheatsheet)

---

## Langage HCL 

HashiCorp Configuration Language

### Concepts

* [`resources`](https://www.terraform.io/language/resources) : objets
* [meta-arguments](https://www.terraform.io/language/meta-arguments/depends_on): arguments de contrôle
    - `count`
    - `depends_on`
    - ...

---

* `data` : données statiques
* [variables](https://www.terraform.io/language/values) : 
    - `input`
    - `local`
    - `output`

* [functions](https://www.terraform.io/language/functions): manipulations
    - `tostring`
    - `tolist`
    - `max`
    - `min`
    - `templatefile`
    - ...

---

* [providers](https://www.terraform.io/language/providers): plugins pour communiquer avec des API externes
* [operators](https://www.terraform.io/language/expressions/operators)

---
#### Ressource 

- ressources = composant de l'infrastructure
- simple ou complexe
- ressource est définie par un provider
- type de ressource préfixé par le nom du provider 


```hcl
resource "provider-name_resource-type" "resource-name" {
    parameter_name = parameter_value
}
```

--- 

Dans un bloc ressource, on peut paramétrer 3 choses : 
- les paramètres de la ressource
- les meta-arguments 
- les provisionners

---

## Les fichiers

```
|-- .terraform         # <-- créé avec la commande terraform init
|-- main.tf            # <-- contient le point d'entrée de la configuration
|-- outputs.tf         # <-- déclaration des variables de sortie
|-- terraform.tfstate  # <-- créé avec la commande terraform apply 
|-- terraform.tf       # <-- configuration de terraform
|-- terraform.tfvars   # <-- valeur des variables d'entrée
|-- variables.tf       # <-- définition des variables d'entrée
```

--- 

Les commande `terraform plan`, `terraform apply`, etc lisent 
l'ensemble des fichiers `*.tf` du dossier
pour définir la déclaration de configuration

---

### priorité des valeurs des variables 

* variables d'environnement (`TF_VAR_nom_variable`)
  ```
  export TF_VAR_external_network
  ```
* `terraform.tfvars`
  ```bash 
  $ cat terraform.tfvars
  external_network = "external"
  external_router = "ext
  ```
* `terraform.tfvars.json`
* `*.auto.tfvars` et `*.auto.tfvars.json`
* les options `-var` et `-var-file`
  ```bash
  $ terraform plan -var 'foo=bar' -var-file=foo.txt
  ```

----

## Usage

### Les 10 commandes les plus utiles

| commande             | description |
| ---------------------|-------------|
| `terraform fmt`      | reformate le code / alignement / indentation |
| `terraform init`     | analyse le code pour installer les providers et modules nécessaires |
| `terraform validate` | vérifie la syntaxe et les versions  |

---

| commande             | description |
| ---------------------|-------------|
| `terraform plan`     | montre ce qui va être effectué |
| `terraform apply`    | applique la configuration |
| `terraform destroy`  | Supprime les ressources  |
| `terraform output`   | affiche les variables de sortie |
| `terraform show`     | affiche l'état chez le provider |
| `terraform state`    | affiche l'état enregistré dans le fichier `tfstate`    |
| `terraform version`  | affiche la version du client terraform |

---


## terraform state : terraform.tfstate

- `.tfstate`
- format json

```json
{                                                        
  "version": 4,
  "terraform_version": "1.1.3",
  "serial": 3,
  "lineage": "e76ee5c5-ffd9-7a0c-e819-9332cfb2b528",
  ...
}
```

---

- création mais aussi mise à jour, suppression
- connaissance des changements 
  * des ressources
  * des paramètres

- très important
- perte => perte du contrôle de l'infra
- stockage local / distant 

---
### Usage 


`terraform state list`

`terraform state show RESSOURCE`

`terraform state rm RESSOURCE`

`terraform import provider_resource_type.resource_name ID`

---

# TD 

* installation de terraform sur VDI
* `td-00-init`
* `td-docker-*`
* `td-os-*`

* utilisation des providers suivant : 
    - [kreuzwerker/docker](https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs)
    - [local](https://registry.terraform.io/providers/hashicorp/local/latest/docs)
    - [terraform-provider-openstack/openstack](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs)

---

## Installation 

* Récupération du binaire
* décompression 
* ajout dans un dossier de la liste des dossiers contenant des exécutables
* lancement de la commande pour valider l'installation

---


```bash
$ git clone https://gitlab.com/ms-icd/terraform-intro.git
$ cd terraform-intro/tp-00-init
$ bash init.sh
$ source ~/.profile  # pour activer l'ajout du dossier ~/bin dans le $PATH
```
---

## TD-docker

Par étape on va déployer des conteneurs sur notre machine VDI
1. un conteneur permettant, via une page web, d'afficher le hostname et les adresses IP
2. utilisation de la commande `terraform import`
2. utilisation de count pour déployer 3 conteneurs
3. utilisation de `input` pour définir le nombre de conteneurs à lancer
4. suppression des ports externes
5. utilisation du Load Balancer [caddy](https://caddyserver.com/docs/)


---

## td-docker-01

```
$ cd td-docker-01
$ cat docker-compose.yml
$ docker-compose up -d 
$ docker ps
$ docker-compose rm -sfv 
```

```
$ cat main.tf
$ terraform init
$ terraform plan 
$ terraform apply --auto-approve
$ docker ps 
$ curl -L localhost:8096
$ terraform destroy
```

---

## utilisation de la commande import

```
$ docker ps -a
$ docker-compose up -d 
$ terraform show 
$ docker ps -f name=web01 --no-trunc
$ ID_CONTAINER=$(docker ps -f name=web01 -q --no-trunc)
$ terraform import docker_container.web $ID_CONTAINER
```
---

## utilisation du meta argument count et de la variable input

```
cd td-docker-02
cp main.tf{.sample,}
terraform init
# modification du fichier main.tf
terraform validate
terraform plan
terraform apply -auto-approve
```

* modifier le fichier main.tf pour avoir 3 conteneurs
* modifier le fichier pour que le nombre de conteneur soit demandé lors de la commande `terraform plan`
* utiliser un fichier terraform.tfvars
* modification pour supprimer l'exposition des ports
---
## Utilisation d'un load-balancer

L'infrastructure à déployer se compose d'un load-balancer (`caddy`) et de X serveur web présentant
une page contenant le nom du serveur.  

* S'appuyer sur le fichier `td-docker-03/docker-compose.yml` pour déclarer notre infrastructure
* S'appuyer sur le fichier de configuration de `td-docker-03/caddy/Caddyfile` pour configurer le load-balancer
* X est la valeur d'une variable d'entrée (`inputs`)
--- 

#### Point d'attention 

* on n'est pas dans un docker-compose alors il n'y a pas de résolution interne des nom de conteneur
* pour utiliser l'argument `volume` de la ressource `docker_container` il faut un chemin absolu vers le dossier contenant le fichier de configuration or cette solution n'est pas _portable_.

---

#### Coup de pouce

* Récupération des IP pour les afficher en sortie : 

```
output "web_ip" {
  value = "${docker_container.web.*.ip_address}"
}
```

---

* Utilisation de templatefile 
```
content = templatefile("${path.module}/caddy/Caddyfile.tfpl"
  {
      ips = docker_container.web.*.ip_address
    }
  )
```
  - Présentation d'un modèle : 
  ```
    :80 {
      reverse_proxy * {
        %{ for addr in ips ~}
        to ${addr}:8080
        %{ endfor ~}
    
        lb_policy round_robin
        lb_try_duration 1s
        lb_try_interval 250ms
      }
    }
  ```
---

* utilisation du provider local
```
resource "local_file" "caddyfile" {
  content = templatefile("${path.module}/caddy/Caddyfile.tfpl", 
    {
      ips = docker_container.web.*.ip_address
    }
  )
  filename = "./caddy/Caddyfile"
  depends_on = [
    docker_container.web,
  ]
}
```

* construction d'une image docker 
```
resource "docker_image" "caddy" {
  name = "caddy"
  build {
    path = "./caddy"
    tag  = ["caddy:dev"]
  }
  depends_on = [
    local_file.caddyfile,
  ]    
}
```