---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->
![bg left:40% 100%](img/terraform.png)



## Terraform - Introduction


-cedric.roger@seanops.com-

[Support de Cours](https://gitlab.com/ms-icd/terraform-intro)

---

# Licence informations


Auteur : cedric.roger@seanops.com

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)


---

## Référence

- [Documentation Terraform - HashiCorp](https://www.terraform.io/docs/index.html)
- [Getting started with terraform - PacktPub](https://www.packtpub.com/product/getting-started-with-terraform-second-edition/9781788623537)
- [Terraform cookbook - PacktPub](https://www.packtpub.com/product/terraform-cookbook/9781800207554)
  

---

## Sommaire

- Infrastructure moderne
- Installation et premiers usages
- variables
- Modules 
- Integration avec d'autres outils


---

## Infrastructures modernes 

* nombre de serveur augmente
* gestion manuelle ok pour 1 ou 2 douzaine(s) de serveurs
* pour plusieurs centaines 
* plus que de simple de serveurs : il faut ajouter toutes les application en mode SaaS 


---
## Infrastructures modernes : IaC

IaC = définition de son infrastructure comme le code d'un logiciel 

utilisation des mêmes méthodes : 
- code source
- collaboration
- versionning
- tests 
- intégration continue 
- déploiement continue

---
## Automatisation

deux manières : 
 - procédurale
 - déclarative

---
## Procédurale

script avec commandes

Exemple : livraison d'un serveur avec nginx avec notification par email : 

```bash
#!/bin/bash

set -x

if [ ! $# -eq 1 ]; then 
    exit 1
fi

SITE=$ARG1

apt-get install nginx mailx -y 
sed -i "s/default/$SITE/" /etc/nginx/nginx.conf
systemctl enable nginx
systemctl restart nginx
echo "Successfull configuration of site $SITE" | mail -s "$hostname delivery" cedric.roger@seanops.com

exit 0
```

---
## Prcédurale

Il faut savoir **comment faire**

Autre exemple : imposer une résolution DNS statique pour une API dans le lan

```bash
echo "192.168.0.5 api.seanops.com" >> /etc/hosts
```

Quid : 
- exécution multiple
- modification d'une valeur

---
## Déclarative

On écrit ce qui doit être, ainsi si l'état voulu est déjà en place l'outils de gestion ne ré-exécute pas la tâche. 
l'outil de gestion ca assurer que l'état de l'infra est celui défini dans le code

Exemple : déclaration dns statique avec Puppet : 

```json
host { 
    'api.seanops.com': 
        ip => '192.168.0.5';
}
```

---
## Déclarative

**Agnosticisme de la plateforme**

exemple pour installer un nginx sur un serveur via ansible : 
```yaml
- name: installation nginx
  package: 
    name: nginx
    state: latest
```
C'est ansible qui va décider d'utiliser yum ou dnf ou apt pour installer le paquet

---
### Gestion de configuration 

Liste non exaustive d'outils utilisés pour cette méthode déclarative : 
- Puppet
- chef
- ansible
- saltstack
- ...

Quid: 
 - ok pour de la configuration mais pour le déploiement de ressources ? 


---
## Critères

- support pour un large choix de service
- indempotence 
- gestion des dépendances de ressources
- intégration avec d'autres outils
- agnosticisme des plateformes
- extension facile (ajout de nouveau type de ressource)

---
## Fournisseurs

Les fournisseurs de ressources sont essentiellement AWS / GCP / MS AZure / Openstack / DigitalOcean
mais il y en a d'autres comme des fournisseurs de services 
 - DNS
 - VPN
 - téléphonie
 - email

2 méthodes pour créer une ressource : 
 - ClickOPS
 - script qui requête une API 

---

## Utilisation des outils de gestion de configuration ? 

### Outils de gestion de configuration : 
 - manque de support pour certaines ressources
 - définition workflows : ajout règle de pare-feu pour autoriser une application à discuter avec un serveur BDD
 -> dans quel module ajoute-t-on la gestion de cette règle au niveau de l'application, au niveau de la bdd ?
 - lieu de l'exécution de cet outil de management ? sur une instance AWS ? ou positionner les clés de distribution ?
   est-ce compatible avec du CI/CD ? 

### Outils lié à un fournisseur : cloudformation / heat
 - trop spécifique 

---
## Terraform

- Juillet 2014
- HashiCorp (Vagrant / Packer / Consul)
- Open source https://github.com/hashicorp/terraform
- Il existe une solution payante offrant une partie hébergée et qui répond à des problématiques non résolues par la partie opensource (stockage distant de l'état, autorisations (ACP), notifications, etc )
- support de + 40 fournisseurs ( développeurs HashiCorp et communauté)
- langage Go
- version actuelle : 1.1.3
