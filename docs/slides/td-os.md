---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->
![bg left:40% 100%](img/terraform.png)



## Terraform - TD OS


-cedric.roger@seanops.com-

---

# Licence informations


Auteur : cedric.roger@seanops.com

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)


---


## TD Openstack 

1. Déployer une instance nattée
2. Déployer une instance + une ip flottante publique
3. Utilisation des variables
4. Déployer deux instances en utilisant un module
5. Déployer deux instances et les configurer avec Ansible

---

### TD-OS-01

Déployer une instance Openstack reliée au réseau externe via un NAT. 

Ressources nécessaires : 
* `openstack_compute_instance_v2`

Arguments : 
* `name`
* `image_name`
* `flavor_name`
* `security_groups`
* `user_data`
* `network`

---

Commandes : 

```bash
cd td-os-01-instance
# jeter un oeil aux fichier pour savoir ce qui est fait
terraform init
terraform plan 
terraform apply --auto-remove
terraform show 
```
Aller sur l'interface Horizon pour : 
- visualiser l'instance déployée
- valider l'application des `user_data` en se connectant en console

Rappel: bien faire un `source` de son fichier openstack-rc

---

### TD-OS-02

Déployer une instance accessible depuis le reseau externe

Resources nécessaires : 
* `openstack_compute_instance_v2`
* `openstack_compute_keypair_v2`
* `openstack_networking_network_v2`
* `openstack_networking_subnet_v2`
* `openstack_networking_router_interface_v2`
* `openstack_networking_floatingip_v2`
* `openstack_compute_floatingip_associate_v2`
* `openstack_compute_secgroup_v2`

---

Commandes : 

```bash
cd td-os-02-instance-reseau
# jeter un oeil aux fichier pour savoir ce qui est fait
# ajout d'un fichier terraform.tfvars avec les bonnes données
terraform init
terraform plan 
terraform apply --auto-remove
terraform output
# lancer une connexion ssh pour valider le bon accès à l'instance
ssh user@$(terraform output  --raw  ext_IP) hostnamectl status
```

---

Pour aller plus loin : 

- utiliser le _provisioner_ `local_exec` pour effectuer la connexion ssh
- utiliser le _provisioner_ `remote_exec` pour afficher le nom du serveur


---

### TD-OS-03

Utiliser au maximum les variables

Reprendre le contenu du TD-OS-03 et remplacer toutes les données statiques en variables : 

- nom de l'instance
- image
- modèle 
- sous-réseau
- nom de la paire de clé 
- règle de sécurité
- nom du réseau

---

### TD-OS-04

Faire un module 

Rappel : un module est un ensemble de ressources à utiliser ensemble

1. Créer un dossier `os-ext-instance` ayant le même contenu que le dossier TD-OS-03
2. Dans le dossier parent, créer un fichier `main.tf` utilisant le module td-os-03 renommée en `os-ext-instance`
3. Ajouter un fichier `terraform.tfvars`
4. Déployer l'instance
5. Déployer deux instances 

--- 

### TD-OS-05

Utilisation d'Ansible pour configurer les instances déployées. 

Objectif: on veut obtenir l'infrastucture suivante : 

```
            visiteur                visiteur
               |                        |
               |                        |
           ---------------        --------------
           |   HAPorxy    |       |  HAProxy   |
           |     |\       |       |/   |       |
           |     | \--------------/-- NGINX    |
           |     |        |      /|            |
           |  NGINX-------------/ |            |
           ----------------       --------------


```
---

Pour ce faire il faut : 
- Créer un inventaire à partir des variables de sortie
- Exécuter le playbook Ansible via 
    * `null_resource`
    * `local_exec`

Action du playbook :  
 - Installation et configuration de HAProxy 
 - Installation et configuration de NGINX
 - Modification d'une page HTML
