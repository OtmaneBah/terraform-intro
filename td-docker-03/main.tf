terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
    local = {
      source = "local"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_image" "nginx" {
  name = "nginx:latest"
}

resource "docker_image" "goif" {
  name = "flowerpot/goif"
}



resource "docker_image" "caddy" {
  name = "caddy"
  build {
    path = "./caddy"
    tag  = ["caddy:dev"]
  }
  depends_on = [
    local_file.caddyfile,
  ]    
}

resource "docker_container" "lb" {
  name  = "lb"
  image = docker_image.caddy.latest
  #dns = ["127.0.0.11"]
  ports {
    internal = "80"
    external = "8096"
  }
  #volumes {
  #  container_path = "/etc/Caddyfile"
  #  host_path = "./caddy/Caddyfile"
  #}
  
  depends_on = [
    docker_image.caddy,
  ]
}

resource "docker_container" "web" {
  count = 3
  name  = "web${count.index}"
  image = docker_image.goif.latest

  #ports {
  #  internal = "8080"
  #  external = "808${count.index}"
  #}
}

output "web_ip" {
  value = "${docker_container.web.*.ip_address}"
}


resource "local_file" "caddyfile" {
  content = templatefile("${path.module}/caddy/Caddyfile.tfpl", 
    {
      ips = docker_container.web.*.ip_address
    }
  )
  filename = "./caddy/Caddyfile"
  depends_on = [
    docker_container.web,
  ]
}
