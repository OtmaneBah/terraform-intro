#!/bin/bash

set -e 

if [  $# -eq 1 ]; then
  ssh_key_name=$1
else 
  ssh_key_name="CHANGEIT"
fi

if [ -e ~/.openstack-rc ]; then 
  source ~/.openstack-rc
fi 

sed -i "s/<SSH_PUBLIC_KEY_NAME>/$ssh_key_name/" instance.tf

terraform init

terraform validate 

terraform apply 

exit 0
