terraform {
    required_version = ">= 0.14.0"
    required_providers {
        openstack = {
            source = "terraform-provider-openstack/openstack"
            version = "~> 1.35.0"
        }
    }
}

provider openstack {
#    region = "regionOne"
}
resource "openstack_compute_instance_v2" "standalone-server" {
  name              = "std_server-2"
  image_name        = "imta-ubuntu"
  flavor_name       = "m1.small"
  key_pair          = "<SSH_PUBLIC_KEY_NAME>"
  security_groups   = ["default"]
  user_data         = file("./user_data.yaml")

  metadata = {
      this = "that"
  }

  network {
      name = "ms-icd-intro_03_Network"
  }
}
