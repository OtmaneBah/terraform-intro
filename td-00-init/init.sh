#!/bin/bash

# tp_init 

TF_VERSION=1.1.3

echo "Preparing directories environment ..." 
if [ ! -d ~/bin ]; then 
  mkdir ~/bin
fi

echo "Downloading terraform binary ..."
if [ ! -e terraform_${TF_VERSION}_linux_amd64.zip ]; then  
  curl -O https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip
fi
if [ ! -x ~/bin/terraform ]; then 
  unzip terraform_${TF_VERSION}_linux_amd64.zip -d ~/bin/
fi
rm -f terraform_${TF_VERSION}_linux_amd64.zip

echo ""
terraform -v 

echo -e "\nCreate a void template in tp_init directory "

if [ ! -d ~/terraform/tp_init/ ]; then
  mkdir -p ~/terraform/tp_init/
fi

touch ~/terraform/tp_init/void.tf 

pushd ~/terraform/tp_init/
terraform apply 
popd 

rm -rf ~/terraform/tp_init/

exit 0